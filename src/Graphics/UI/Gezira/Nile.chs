module Graphics.UI.Gezira.Nile
  (
    NileStatus(..)
  , LoguePrim
  , Prologue
  , toLoguePrim
  , nile_startup
  , nile_shutdown
  , nile_sync
  , nile_status
  , nile_error
  , nile_print_leaks
  , nile_Process_feed
  , nile_Process_gate
  , nile_Process_pipe_v
  , nile_Capture
  , nile_Reverse
  , nile_SortBy
  , nile_DupZip
  , nile_DupCat
  , nile_Funnel
  , nile_Funnel_pour
  , nile_Process
  )
where

#include "nile.h"
import Foreign.C
import Foreign
import Foreign.Marshal.Alloc
import System.IO.Unsafe
import Graphics.UI.Gezira.Gezira

#c
enum NileStatus {
   NileStatusOk = NILE_STATUS_OK,
   NileStatusOutOfMemory = NILE_STATUS_OUT_OF_MEMORY,
   NileStatusBadArg = NILE_STATUS_BAD_ARG,
   NileStatusShuttingDown = NILE_STATUS_SHUTTING_DOWN
};
#endc
{#enum NileStatus {} deriving (Show, Eq) #}

type LoguePrim = Ptr () -> Ptr () -> IO (Ptr ())
type BodyPrim = Ptr () -> Ptr () -> Ptr () -> IO (Ptr ())
type Prologue = IO ()
foreign import ccall "wrapper"
  mkLogue :: LoguePrim -> IO (FunPtr LoguePrim)

toLoguePrim :: Prologue -> IO (FunPtr LoguePrim)
toLoguePrim prologue =
  mkLogue
  (
     \_ nbOutPointer ->
         prologue >> return nbOutPointer
  )

cToEnum :: Enum a => CInt -> a
cToEnum = toEnum . fromIntegral

cToBool :: CInt -> Bool
cToBool x | x > 0 = True
cToBool _ = False

{- |
@
nile_Process_t *
nile_startup (char *memory, int nbytes, int nthreads);
@
-}

{#fun nile_startup {id `Ptr CChar', `CInt', `CInt'} -> `NileProcess' nileProcessFromC #}

{- |
@
char *
nile_shutdown (nile_Process_t *init);
@
-}
{#fun nile_shutdown {nileProcessToC `NileProcess' } -> `CString' #}

{- |
@
void
nile_sync (nile_Process_t *init);
@
-}
{#fun nile_sync {nileProcessToC `NileProcess' } -> `()' #}

{- |
@
nile_Status_t
nile_status (nile_Process_t *init);
@
-}
{#fun nile_status {nileProcessToC `NileProcess' } -> `NileStatus' cToEnum #}

{- |
@
bool
nile_error (nile_Process_t *init);
@
-}
{#fun nile_error {nileProcessToC `NileProcess' } -> `Bool' cToBool #}


{- |
@
void
nile_print_leaks (nile_Process_t *init);
@
-}
{#fun nile_print_leaks {nileProcessToC `NileProcess' } -> `()' #}

{- |
@
nile_Process_t *
nile_Process_pipe (nile_Process_t *p1, ...);
@
-}

{- |
@
void
nile_Process_feed (nile_Process_t *p, float *data, int n);
@
-}
{#fun nile_Process_feed {nileProcessToC `NileProcess', id `Ptr CFloat', `CInt' } -> `()' #}

{- |
@
void
nile_Process_gate (nile_Process_t *gater, nile_Process_t *gatee);
@
-}
{#fun nile_Process_gate {nileProcessToC `NileProcess', nileProcessToC `NileProcess'} -> `()' #}

{- |
@
nile_Process_t *
nile_Process_pipe_v (nile_Process_t **ps, int n);
@
-}
{#fun nile_Process_pipe_v as nile_Process_pipe_v' {id `Ptr (Ptr ())', `CInt'} -> `NileProcess' nileProcessFromC #}

nile_Process_pipe_v :: [NileProcess] -> IO NileProcess
nile_Process_pipe_v processes =
   withArray
     (map nileProcessToC processes)
     (
       \pipelinePointer ->
         nile_Process_pipe_v' pipelinePointer (fromIntegral (length processes))
     )

{- |
@
nile_Process_t *
nile_Capture (nile_Process_t *parent, float *data, int *n, int size);
@
-}
{#fun nile_Capture {nileProcessToC `NileProcess', id `Ptr CFloat', id `Ptr CInt', `CInt' } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
nile_Reverse (nile_Process_t *parent, int in_quantum);
@
-}
{#fun nile_Reverse {nileProcessToC `NileProcess', `CInt' } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
nile_SortBy (nile_Process_t *p, int quantum, int index);
@
-}
{#fun nile_SortBy {nileProcessToC `NileProcess', `CInt', `CInt' } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
nile_DupZip (nile_Process_t *p,  int quantum,
             nile_Process_t *p1, int p1_out_quantum,
             nile_Process_t *p2, int p2_out_quantum);
@
-}
{#fun nile_DupZip {nileProcessToC `NileProcess', `CInt', nileProcessToC `NileProcess', `CInt', nileProcessToC `NileProcess', `CInt' } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
nile_DupCat (nile_Process_t *p,  int quantum,
             nile_Process_t *p1, int p1_out_quantum,
             nile_Process_t *p2, int p2_out_quantum);
@
-}
{#fun nile_DupCat {nileProcessToC `NileProcess', `CInt', nileProcessToC `NileProcess', `CInt', nileProcessToC `NileProcess', `CInt' } -> `NileProcess' nileProcessFromC #}

{- |
@
nile_Process_t *
nile_Funnel (nile_Process_t *parent);
@
-}
{#fun nile_Funnel {nileProcessToC `NileProcess'} -> `NileProcess' nileProcessFromC #}

{- |
@
void
nile_Funnel_pour (nile_Process_t *p, float *data, int n, int EOS);
@
-}
{#fun nile_Funnel_pour {nileProcessToC `NileProcess', id `Ptr CFloat', `CInt', `CInt'} -> `()' #}

{- |
@
nile_Process_t *
nile_Process (nile_Process_t *p, int quantum, int sizeof_vars,
              nile_Process_logue_t prologue,
              nile_Process_body_t  body,
              nile_Process_logue_t epilogue);

@
-}
{#fun nile_Process as nile_Process' {
                                     nileProcessToC `NileProcess'
                                    , `CInt'
                                    , `CInt'
                                    , id `FunPtr LoguePrim'
                                    , id `FunPtr BodyPrim'
                                    , id `FunPtr LoguePrim'
                                    } -> `NileProcess' nileProcessFromC #}
nile_Process :: NileProcess -> CInt ->  Maybe (FunPtr LoguePrim) -> IO NileProcess
nile_Process old quantum prologue = do
  let prologuePointer = maybe nullFunPtr id prologue
  nile_Process' old quantum 0 prologuePointer nullFunPtr nullFunPtr
