{-# LANGUAGE CPP #-}
import Distribution.Simple
import Distribution.Simple.LocalBuildInfo
import Distribution.Simple.Setup
import Distribution.Simple.Utils
import Distribution.Simple.BuildPaths
import Distribution.System
import Distribution.PackageDescription as PD
import qualified Distribution.Simple.Register as Register
import Distribution.Verbosity
import Distribution.InstalledPackageInfo
import Distribution.Simple.Register
import System.Directory(getCurrentDirectory, withCurrentDirectory)
import System.FilePath ((</>), (<.>), FilePath, takeDirectory)
import System.Environment (getEnv, setEnv)
import System.IO.Error(tryIOError)
import Data.Maybe
import Distribution.Text ( display )
import Data.List

-- cabal >=2.0.1.1
#if defined(MIN_VERSION_Cabal) && MIN_VERSION_Cabal(2,0,1)
_registerPackage verbosity lbi packageDbs installedPkgInfo
  = registerPackage verbosity (compiler lbi) (withPrograms lbi) packageDbs installedPkgInfo Register.defaultRegisterOptions
#else
_registerPackage verbosity lbi packageDbs installedPkgInfo
  = registerPackage verbosity (compiler lbi) (withPrograms lbi) False {- multiinstance -} packageDbs installedPkgInfo
#endif

staticLibExtension :: String
staticLibExtension =
  case buildOS of
    Windows -> "lib"
    _       -> "a"

dynamicLibExtension :: String
dynamicLibExtension =
  case buildOS of
    OSX -> "dylib"
    _   -> "so"

main =
  defaultMainWithHooks
    simpleUserHooks
      {
        preConf = myPreConf
      , buildHook = myBuildHook
      , copyHook = myCopyHook
      , cleanHook = myCleanHook
      , regHook = myRegisterHook
      }

runMake :: [String] -> IO ()
runMake args =
  case buildOS of
    os | os `elem` [FreeBSD, OpenBSD, NetBSD, DragonFly]
      -> rawSystemExit normal "gmake" args
    _ -> rawSystemExit normal "make" args

data WithCDeps = Build | Clean

nileDirectory :: FilePath -> FilePath
nileDirectory pwd = pwd </> "nile" </> "runtimes" </> "c"

geziraDirectory :: FilePath -> FilePath
geziraDirectory pwd = pwd </> "gezira" </> "c"

withCDeps :: WithCDeps -> IO ()
withCDeps withCDeps = do
  pwd <- getCurrentDirectory
  mapM_
    (
      \d ->
        withCurrentDirectory
          d
          (case withCDeps of
             Build -> runMake ["-f", "Makefile.gcc"]
             Clean -> runMake ["-f", "Makefile.gcc", "clean"]
          )
    )
    [nileDirectory pwd , geziraDirectory pwd]

myPreConf :: Args -> ConfigFlags -> IO HookedBuildInfo
myPreConf args flags = do
  withCDeps Build
  preConf simpleUserHooks args flags

updateEnv :: String -> String -> IO ()
updateEnv env value = do
  old <- tryIOError (getEnv env)
  setEnv env ((either (const value)
                      (\old' -> value ++
                               (case buildOS of
                                  Windows -> ";"
                                  _ -> ":") ++
                               old'
                      )
                      old))

addCLibs :: FilePath -> FilePath -> PackageDescription -> PackageDescription
addCLibs nileDirectory geziraDirectory pd =
    let cLibsAdded = do
        lib <- library pd
        let ld = (PD.ldOptions (libBuildInfo lib)) ++
                   [
                     nileDirectory  </> "libnile" <.> Main.staticLibExtension
                   , geziraDirectory </> "libgezira" <.> Main.staticLibExtension
                   ]
        return (lib { libBuildInfo = (libBuildInfo lib) { PD.ldOptions = ld }})
    in pd { library = cLibsAdded }

myBuildHook :: PackageDescription -> LocalBuildInfo -> UserHooks -> BuildFlags -> IO ()
myBuildHook pd lbi hooks flags = do
  pwd <- getCurrentDirectory
  buildHook simpleUserHooks (addCLibs (nileDirectory pwd) (geziraDirectory pwd) pd) lbi hooks flags

myCopyHook :: PackageDescription -> LocalBuildInfo -> UserHooks -> CopyFlags -> IO ()
myCopyHook pd lbi uh flags = do
  copyHook simpleUserHooks pd lbi uh flags
  pwd <- getCurrentDirectory
  let installDirs = absoluteInstallDirs pd lbi
                    . fromFlag . copyDest
                    $ flags
      libPref = libdir installDirs
  rawSystemExit (fromFlag $ copyVerbosity flags) "cp"
      [(nileDirectory pwd) </> "libnile" <.> Main.staticLibExtension , libPref]
  rawSystemExit (fromFlag $ copyVerbosity flags) "cp"
      [(nileDirectory pwd) </> "libnile-dyn" <.> dynamicLibExtension , libPref]
  rawSystemExit (fromFlag $ copyVerbosity flags) "cp"
      [(geziraDirectory pwd) </> "libgezira" <.> Main.staticLibExtension , libPref]
  rawSystemExit (fromFlag $ copyVerbosity flags) "cp"
      [(geziraDirectory pwd) </> "libgezira-dyn" <.> dynamicLibExtension , libPref]
  case buildOS of
    OSX -> updateEnv "DYLD_LIBRARY_PATH" (takeDirectory libPref)
    _ -> updateEnv "LIBRARY_PATH" (takeDirectory libPref)

myCleanHook pd x uh cf = do
  withCDeps Clean
  cleanHook simpleUserHooks pd x uh cf

-- Based on code in "Gtk2HsSetup.hs" from "gtk" package
myRegisterHook pkg_descr localbuildinfo _ flags =
    if hasLibs pkg_descr
    then myRegister pkg_descr localbuildinfo flags
    else setupMessage verbosity
           "Package contains no library to register:" (packageId pkg_descr)
  where verbosity = fromFlag (regVerbosity flags)

myRegister :: PackageDescription -> LocalBuildInfo -> RegisterFlags -> IO ()
myRegister pkg@PackageDescription { library = Just lib } lbi regFlags = do
    let clbi = getComponentLocalBuildInfo lbi CLibName
    installedPkgInfoRaw' <- generateRegistrationInfo verbosity pkg lib lbi clbi inplace False distPref packageDb
    let installedPkgInfo = installedPkgInfoRaw' {
                                libraryDynDirs = (libraryDynDirs installedPkgInfoRaw') ++ (libraryDirs installedPkgInfoRaw'),
                                -- this is what this whole register code is all about
                                extraGHCiLibraries =
                                  case buildOS of
                                    Windows -> ["libnile-dyn", "libgezira-dyn"]
                                    _ -> ["nile-dyn", "gezira-dyn"]
                                }

     -- Three different modes:
    case () of
     _ | modeGenerateRegFile   -> writeRegistrationFile installedPkgInfo
       | modeGenerateRegScript -> die "Generate Reg Script not supported"
       | otherwise             ->
         _registerPackage verbosity lbi packageDbs installedPkgInfo
  where
    modeGenerateRegFile = isJust (flagToMaybe (regGenPkgConf regFlags))
    regFile             = fromMaybe (display (packageId pkg) <.> "conf")
                                    (fromFlag (regGenPkgConf regFlags))
    modeGenerateRegScript = fromFlag (regGenScript regFlags)
    inplace   = fromFlag (regInPlace regFlags)
    packageDbs = nub $ withPackageDB lbi
                    ++ maybeToList (flagToMaybe  (regPackageDB regFlags))
    packageDb = registrationPackageDB packageDbs
    distPref  = fromFlag (regDistPref regFlags)
    verbosity = fromFlag (regVerbosity regFlags)

    writeRegistrationFile installedPkgInfo = do
      notice verbosity ("Creating package registration file: " ++ regFile)
      writeUTF8File regFile (showInstalledPackageInfo installedPkgInfo)
